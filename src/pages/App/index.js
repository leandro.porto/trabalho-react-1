import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Notes from '../Notes'
import About from '../About'
import './App.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Notes} />
          <Route path="/about" exact component={About} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
