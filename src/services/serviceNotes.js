const result = [
    { text: 'Lavar o carro', id: new Date().getTime() },
    { text: 'Dar vacina na Alice', id: new Date().getTime() }
]

const listNotes = () => {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(result)
        }, 2000)
    })
}


const saveNotes = (text) => {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            result.push({
                text: text,
                id: new Date().getTime()
            })
            resolve()
        }, 2000)
    })
}


module.exports = {
    listNotes,
    saveNotes
}